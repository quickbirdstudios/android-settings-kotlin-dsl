package com.quickbirdstudios.lib.settings;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.graphics.Point;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.trello.lifecycle2.android.lifecycle.AndroidLifecycle;
import com.trello.rxlifecycle2.LifecycleProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsAdapter extends RecyclerView.Adapter<SettingsViewHolder> {

    private static final int TYPE_EDIT_ITEM = 0;
    private static final int TYPE_HEADER_ITEM = 1;
    private static final int TYPE_BUTTON_ITEM = 2;
    private static final int TYPE_TOGGLE_ITEM = 3;
    private static final int TYPE_TITLE_ITEM = 4;
    private static final int TYPE_EDIT_POINT = 5;
    private static final int TYPE_LABEL_ITEM = 6;

    private final List<AbstractSettingsItem> items;

    SettingsAdapter(List<AbstractSettingsItem> items) {
        this.items = items;
    }

    public static Builder builder(Context context) {
        return new Builder(context);
    }

    public void save() {
        for (AbstractSettingsItem item : items) {
            item.save();
        }
    }

    @Override
    public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_EDIT_ITEM:
                return SettingsEditItemViewHolder.newInstance(parent);

            case TYPE_HEADER_ITEM:
                return SettingsHeaderViewHolder.newInstance(parent);

            case TYPE_BUTTON_ITEM:
                return SettingsButtonViewHolder.newInstance(parent);

            case TYPE_TOGGLE_ITEM:
                return SettingsToggleItemViewHolder.newInstance(parent);

            case TYPE_TITLE_ITEM:
                return SettingsTitleItemViewHolder.newInstance(parent);

            case TYPE_EDIT_POINT:
                return SettingsEditPointItemViewHolder.newInstance(parent);

            case TYPE_LABEL_ITEM:
                return SettingsLabelViewHolder.newInstance(parent);
        }

        throw new IllegalArgumentException();
    }

    @Override
    public int getItemViewType(int position) {
        AbstractSettingsItem item = items.get(position);
        if (item instanceof SettingsEditItem) return TYPE_EDIT_ITEM;
        if (item instanceof SettingsHeaderItem) return TYPE_HEADER_ITEM;
        if (item instanceof SettingsButtonItem) return TYPE_BUTTON_ITEM;
        if (item instanceof SettingsToggleItem) return TYPE_TOGGLE_ITEM;
        if (item instanceof SettingsTitleItem) return TYPE_TITLE_ITEM;
        if (item instanceof SettingsEditPointItem) return TYPE_EDIT_POINT;
        if (item instanceof SettingsLabelItem) return TYPE_LABEL_ITEM;
        else throw new IllegalStateException("Unknown SettingsItem");
    }

    @Override
    public void onBindViewHolder(SettingsViewHolder holder, int position) {
        items.get(position).bind(holder);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public interface SectionBuilderLambda {
        void buildSection(SectionBuilder section);
    }

    public static class Builder {
        private final Context context;
        private final List<AbstractSettingsItem> items = new ArrayList<>();

        private Builder(Context context) {
            this.context = context;
        }

        public Builder newSection(String title, SectionBuilderLambda builderLambda) {
            SectionBuilder sectionBuilder = new SectionBuilder(this, title);
            builderLambda.buildSection(sectionBuilder);
            return this;
        }

        public Builder newSection(@StringRes int title, SectionBuilderLambda builderLambda) {
            String titleString = context.getString(title);
            SectionBuilder sectionBuilder = new SectionBuilder(this, titleString);
            builderLambda.buildSection(sectionBuilder);
            return this;
        }

        public Builder addItem(AbstractSettingsItem item) {
            this.items.add(item);
            return this;
        }


        public SettingsAdapter build() {
            return new SettingsAdapter(items);
        }

    }

    public static class SectionBuilder {
        private final Builder builder;

        private SectionBuilder(Builder builder,
                               String title) {
            this.builder = builder;
            builder.items.add(new SettingsHeaderItem(title));
        }


        public EditItemBuilder newItem(SettingType type) {
            return new EditItemBuilder(this, type);
        }

        public LabelItemBuilder newLabel() {
            return new LabelItemBuilder(this);
        }

        public ButtonItemBuilder newButton() {
            return new ButtonItemBuilder(this);
        }

        public ToggleItemBuilder newToggle() {
            return new ToggleItemBuilder(this);
        }

        public PointEditItemBuilder newPointItem() {
            return new PointEditItemBuilder(this);
        }


    }

    public  static class PointEditItemBuilder {
        private final SectionBuilder sectionBuilder;
        private final SettingsEditPointItem item = new SettingsEditPointItem();

        private PointEditItemBuilder(SectionBuilder sectionBuilder) {
            this.sectionBuilder = sectionBuilder;
        }

        public PointEditItemBuilder label(String label) {
            item.label = label;
            return this;
        }

        public PointEditItemBuilder label(@StringRes int label) {
            item.label = sectionBuilder.builder.context.getString(label);
            return this;
        }

        public PointEditItemBuilder value(Point value) {
            item.point = value;
            return this;
        }

        public PointEditItemBuilder onSave(SettingSave.Point saver) {
            item.saver = saver;
            return this;
        }

        public void buildItem() {
            sectionBuilder.builder.addItem(item);
        }

    }

    public static class ToggleItemBuilder {
        private final SectionBuilder sectionBuilder;
        private final SettingsToggleItem item = new SettingsToggleItem();

        private ToggleItemBuilder(SectionBuilder sectionBuilder) {
            this.sectionBuilder = sectionBuilder;
        }

        public ToggleItemBuilder label(String label) {
            item.label = label;
            return this;
        }

        public ToggleItemBuilder label(@StringRes int label) {
            item.label = sectionBuilder.builder.context.getString(label);
            return this;
        }

        public ToggleItemBuilder value(boolean value) {
            item.state = value;
            return this;
        }

        public ToggleItemBuilder onSave(SettingSave.Boolean saver) {
            item.saver = saver;
            return this;
        }

        void buildItem() {
            sectionBuilder.builder.items.add(item);
        }
    }

    public static class ButtonItemBuilder {
        private final SectionBuilder sectionBuilder;
        private final SettingsButtonItem item = new SettingsButtonItem();

        private ButtonItemBuilder(SectionBuilder sectionBuilder) {
            this.sectionBuilder = sectionBuilder;
        }

        public ButtonItemBuilder label(String label) {
            item.label = label;
            return this;
        }

        public ButtonItemBuilder label(@StringRes int label) {
            item.label = sectionBuilder.builder.context.getString(label);
            return this;
        }

        public ButtonItemBuilder onClick(ClickListener click) {
            item.onClickListener = v -> click.onClick();
            return this;
        }

        public void buildItem() {
            sectionBuilder.builder.items.add(item);
        }

    }


    @SuppressWarnings("unused")
    public static class EditItemBuilder {
        private final SectionBuilder sectionBuilder;
        private final SettingsEditItem item = new SettingsEditItem();

        private EditItemBuilder(SectionBuilder sectionBuilder,
                                SettingType type) {
            this.sectionBuilder = sectionBuilder;
            item.type = type;
        }


        public EditItemBuilder label(String label) {
            item.label = label;
            return this;
        }

        public EditItemBuilder label(@StringRes int label) {
            item.label = sectionBuilder.builder.context.getString(label);
            return this;
        }

        public EditItemBuilder value(String value) {
            item.value = value;
            return this;
        }

        public EditItemBuilder value(int value) {
            item.value = String.valueOf(value);
            return this;
        }

        public EditItemBuilder value(float value) {
            item.value = String.valueOf(value);
            return this;
        }

        public EditItemBuilder value(double value) {
            item.value = String.valueOf(value);
            return this;
        }

        public EditItemBuilder value(ValueLambda<String> valueLambda) {
            item.value = valueLambda.getValue();
            return this;
        }

        public EditItemBuilder value(LifecycleOwner lifecycleOwner, Observable<String> value) {
            final LifecycleProvider<Lifecycle.Event> provider
                    = AndroidLifecycle.createLifecycleProvider(lifecycleOwner);

            value
                    .compose(provider.bindToLifecycle())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(newValue -> {
                        item.value = newValue;
                        item.invalidate();
                    });

            return this;
        }

        public  EditItemBuilder tryValue(SafeValueLambda<String> valueLambda) {
            try {
                item.value = valueLambda.getValue();
            } catch (Throwable throwable) {
                Log.w("SettingsAdapter", "Failed to get value", throwable);
            }

            return this;
        }

        public EditItemBuilder onSave(SettingSave.String saver) {
            item.settingSave = saver;
            return this;
        }

        public EditItemBuilder onSaveFloat(SettingSave.Float saver) {
            item.settingSave = value -> saver.onSave(Float.parseFloat(value));
            return this;
        }

        public EditItemBuilder onSaveInt(SettingSave.Int saver) {
            item.settingSave = value -> saver.onSave(Integer.parseInt(value));
            return this;
        }

        public  EditItemBuilder onSaveDouble(SettingSave.Double saver) {
            item.settingSave = value -> saver.onSave(Double.parseDouble(value));
            return this;
        }


        public void buildItem() {
            sectionBuilder.builder.items.add(item);
        }

    }

    public static class LabelItemBuilder {
        private final SectionBuilder sectionBuilder;
        private final SettingsLabelItem item = new SettingsLabelItem();

        private LabelItemBuilder(SectionBuilder sectionBuilder) {
            this.sectionBuilder = sectionBuilder;
        }

        public LabelItemBuilder label(String label) {
            item.label = label;
            return this;
        }

        public LabelItemBuilder label(@StringRes int label) {
            item.label = sectionBuilder.builder.context.getString(label);
            return this;
        }

        public LabelItemBuilder value(String value) {
            item.value = value;
            return this;
        }

        public LabelItemBuilder value(@StringRes int value) {
            item.value = sectionBuilder.builder.context.getString(value);
            return this;
        }

        public LabelItemBuilder value(LifecycleOwner lifecycleOwner,
                                      Observable<String> observable) {
            final LifecycleProvider<Lifecycle.Event> provider
                    = AndroidLifecycle.createLifecycleProvider(lifecycleOwner);

            observable
                    .compose(provider.bindToLifecycle())
                    .subscribe(value -> {
                        item.value = value;
                        item.invalidate();
                    });

            return this;
        }

        public LabelItemBuilder size(SettingsLabelItem.Size size) {
            item.valueSize = size;
            return this;
        }

        public void build() {
            sectionBuilder.builder.addItem(item);
        }
    }
}
