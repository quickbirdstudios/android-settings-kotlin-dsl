package com.quickbirdstudios.lib.settings;

import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsToggleItemViewHolder extends SettingsViewHolder {

    TextView label;
    SwitchCompat toggle;

    private SettingsToggleItemViewHolder(View itemView) {
        super(itemView);
        label = itemView.findViewById(R.id.label);
        toggle = itemView.findViewById(R.id.toggle);
    }

    static SettingsToggleItemViewHolder newInstance(ViewGroup parent) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_setting_toggle, parent, false);

        return new SettingsToggleItemViewHolder(view);
    }
}
