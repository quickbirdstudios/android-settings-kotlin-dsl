package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsButtonViewHolder extends SettingsViewHolder {
    final Button button;

    private SettingsButtonViewHolder(View itemView) {
        super(itemView);
        button = itemView.findViewById(R.id.button);
    }

    static SettingsButtonViewHolder newInstance(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_settings_button, parent, false);
        return new SettingsButtonViewHolder(view);
    }
}
