package com.quickbirdstudios.lib.settings;

import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.quickbirdstudios.lib.utils.LambdaUtils;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsEditPointItem extends AbstractSettingsItem {

    @Nullable
    String label;

    @NonNull
    Point point = new Point(0, 0);

    @Nullable
    SettingSave.Point saver;


    private TextListener xListener = new TextListener(
            s -> LambdaUtils.tryOrNull(() -> point.x = Integer.parseInt(s)));
    private TextListener yListener = new TextListener(
            s -> LambdaUtils.tryOrNull(() -> point.y = Integer.parseInt(s)));


    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (viewHolder instanceof SettingsEditPointItemViewHolder) {
            SettingsEditPointItemViewHolder vh = (SettingsEditPointItemViewHolder) viewHolder;
            vh.label.setText(label);

            vh.editX.setText(String.valueOf(point.x));
            vh.editY.setText(String.valueOf(point.y));

            vh.editX.addTextChangedListener(xListener);
            vh.editY.addTextChangedListener(yListener);
        }
    }

    @Override
    void onUnbind(RecyclerView.ViewHolder holder) {
        if (holder instanceof SettingsEditPointItemViewHolder) {
            SettingsEditPointItemViewHolder vh = (SettingsEditPointItemViewHolder) holder;
            vh.editX.removeTextChangedListener(xListener);
            vh.editY.removeTextChangedListener(yListener);
        }
    }

    @Override
    void save() {
        super.save();
        if (saver != null) {
            saver.onSave(point);
        }
    }
}
