package com.quickbirdstudios.lib.settings;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsHeaderItem extends AbstractSettingsItem {

    private final String label;

    SettingsHeaderItem(String label) {
        this.label = label;
    }

    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (!(viewHolder instanceof SettingsHeaderViewHolder)) return;
        ((SettingsHeaderViewHolder) viewHolder).label.setText(label);
    }
}
