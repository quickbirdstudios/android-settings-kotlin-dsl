package com.quickbirdstudios.lib.settings;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

abstract class SettingsViewHolder extends RecyclerView.ViewHolder {

    @Nullable
    private AbstractSettingsItem master;

    SettingsViewHolder(View itemView) {
        super(itemView);
    }

    void setMaster(@Nullable AbstractSettingsItem item) {
        if (master != null) {
            master.unbind(this);
        }

        master = item;
    }
}
