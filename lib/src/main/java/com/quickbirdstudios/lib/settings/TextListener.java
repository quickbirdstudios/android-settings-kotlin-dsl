package com.quickbirdstudios.lib.settings;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

class TextListener implements TextWatcher {

    @NonNull
    private final Lambda afterTextChanged;

    TextListener(@NonNull Lambda afterTextChanged) {
        this.afterTextChanged = afterTextChanged;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        afterTextChanged.afterTextChanged(s.toString());
    }

    interface Lambda {
        void afterTextChanged(String string);
    }


}
