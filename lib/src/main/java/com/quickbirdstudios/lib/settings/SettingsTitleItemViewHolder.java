package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsTitleItemViewHolder extends SettingsViewHolder {

    TextView label;

    private SettingsTitleItemViewHolder(View itemView) {
        super(itemView);
        this.label = itemView.findViewById(R.id.label);
    }

    static SettingsTitleItemViewHolder newInstance(ViewGroup parent) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_settings_title, parent, false);

        return new SettingsTitleItemViewHolder(view);
    }
}
