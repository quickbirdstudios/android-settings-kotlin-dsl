package com.quickbirdstudios.lib.settings;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

interface SafeValueLambda<T> {
    String getValue() throws Throwable;
}
