package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
import com.quickbirdstudios.lib.R;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

class SettingsEditItemViewHolder extends SettingsViewHolder {


    final TextView label;
    final EditText edit;

    private SettingsEditItemViewHolder(View itemView) {
        super(itemView);
        label = itemView.findViewById(R.id.label);
        edit = itemView.findViewById(R.id.edit);
    }

    static SettingsEditItemViewHolder newInstance(ViewGroup parent) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_setting_edit,
                parent,
                false);

        return new SettingsEditItemViewHolder(view);
    }

}
