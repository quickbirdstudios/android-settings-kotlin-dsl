package com.quickbirdstudios.lib.settings;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.quickbirdstudios.lib.R;
/**
 * Created by sebastiansellmair on 28.02.18.
 */

class SettingsLabelViewHolder extends SettingsViewHolder {
    TextView label;
    TextView value;

    private SettingsLabelViewHolder(View itemView) {
        super(itemView);
        label = itemView.findViewById(R.id.label);
        value = itemView.findViewById(R.id.value);
    }

    public static SettingsLabelViewHolder newInstance(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_setting_label, parent, false);
        return new SettingsLabelViewHolder(view);
    }
}
