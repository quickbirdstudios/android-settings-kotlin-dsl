package com.quickbirdstudios.lib.settings;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public interface ClickListener {
    void onClick();
}
