package com.quickbirdstudios.lib.settings;

import android.view.View;

/**
 * Created by sebastiansellmair on 05.02.18.
 */

public class SettingsButtonItem extends AbstractSettingsItem {

    String label;
    View.OnClickListener onClickListener;

    @Override
    void onBind(SettingsViewHolder viewHolder) {
        if (!(viewHolder instanceof SettingsButtonViewHolder)) return;
        SettingsButtonViewHolder vh = (SettingsButtonViewHolder) viewHolder;
        vh.button.setText(label);
        vh.button.setOnClickListener(onClickListener);
    }
}
